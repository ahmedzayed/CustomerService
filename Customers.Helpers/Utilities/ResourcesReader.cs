﻿using System.Threading;

namespace Customers.Helpers.Utilities
{
    public class ResourcesReader
    {
        public static bool IsArabic
        {
            get { return Thread.CurrentThread.CurrentUICulture.Name.StartsWith("ar"); }
        }
    }
}
