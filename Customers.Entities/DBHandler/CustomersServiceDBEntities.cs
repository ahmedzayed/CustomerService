﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
    public partial class CustomersServiceDBEntities
    {
        public CustomersServiceDBEntities(int userName)
            : base("name=CustomersServiceContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
            UserName = userName;
        }

        public int UserName
        {
            get;
            set;
        }

        public override int SaveChanges()
        {
            var autoDetectChanges = Configuration.AutoDetectChangesEnabled;
            try
            {
                Configuration.AutoDetectChangesEnabled = false;
                ChangeTracker.DetectChanges();
                foreach (var entry in ChangeTracker.Entries().Where(e =>
                    e.State == System.Data.Entity.EntityState.Added || e.State == System.Data.Entity.EntityState.Modified))
                {
                    Logging.ApplyAudit(entry, UserName);
                }
                var errors = GetValidationErrors().ToList();
                if (errors.Any())
                {
                    var errorMessages = new StringBuilder();
                    errors.ForEach(m => errorMessages.AppendLine(m.ValidationErrors.ToString()));
                    throw new DbEntityValidationException("Validation errors were found during save: " + errorMessages);
                }
                ChangeTracker.DetectChanges();
                Configuration.ValidateOnSaveEnabled = false;
                return base.SaveChanges();
            }
            finally
            {
                Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            }
        }
    }
}
