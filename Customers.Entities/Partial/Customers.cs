﻿using Customers.Model.BasicInput;
using Customers.Model.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
    
   public partial class Customers
    {
        public static Customers Clone(CustomerVm viewModel)
        {
            return new Customers
            {
                Id = viewModel.Id,
                NameAr = viewModel.NameAr,
                NameEn = viewModel.NameEn,
                CreatedDate=viewModel.CreatedDate,
                CustomerActivityId=viewModel.CustomerActivityId,
                CustomerAddress=viewModel.CustomerAddress,
                CustomerWebSite=viewModel.CustomerWebSite,
                Details=viewModel.Details,
                UpdatedBy=viewModel.UpdatedBy,
                UpdatedDate=viewModel.UpdatedDate,
                
                
                CreatedBy = viewModel.CreatedBy,
                
            };
        }
    }
}
