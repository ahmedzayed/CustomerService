﻿using Customers.Model.BasicInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{
    partial class TasksEngineers
    {
        public static TasksEngineers Clone(BasicInputVm viewModel)
        {
            return new TasksEngineers
            {
                Id = viewModel.Id,
                NameAr = viewModel.NameAr,
                NameEn = viewModel.NameEn,
                CreatedBy = viewModel.CreatedBy
            };
        }
    }
}
