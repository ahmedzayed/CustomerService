﻿using Customers.Model.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Entities
{

    public partial class Employees
    {
        public static Employees Clone(EmployeesVm viewModel)
        {
            return new Employees
            {
                Id = viewModel.Id,
                NameAr = viewModel.NameAr,
                NameEn = viewModel.NameEn,
                CreatedDate = viewModel.CreatedDate,
                Nationality = viewModel.Nationality,
                WorkHours = viewModel.WorkHours,
                DateOfAppointment = viewModel.DateOfAppointment,
                FaceNumber = viewModel.FaceNumber,
                Salary=viewModel.Salary,
                SpecializationsId=viewModel.SpecializationsId,
                WorkLocation=viewModel.WorkLocation,
                WorkSystemId=viewModel.WorkSystemId,
                UpdatedBy = viewModel.UpdatedBy,
                UpdatedDate = viewModel.UpdatedDate,
                EmployeeTypeId=viewModel.EmployeeTypeId,
               


                CreatedBy = viewModel.CreatedBy,

            };
        }
    }
}