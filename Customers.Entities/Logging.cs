﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Customers.Entities
{
    public class Logging
    {
        const string CreatedByProperty = "CreatedBy";
        const string CreatedAtProperty = "CreatedDate";

        const string LastModifiedByProperty = "LastModifiedBy";
        const string LastModifiedAtProperty = "LastModifiedDate";

        public static void ApplyAudit(DbEntityEntry entityEntry, int userName)
        {
            var type = entityEntry.Entity.GetType();
            if (entityEntry.State == EntityState.Added)
            {
                if (type.GetProperty(CreatedByProperty) != null)
                {
                    var createdByProperty = entityEntry.Property(CreatedByProperty);
                    createdByProperty.CurrentValue = userName;
                }
                if (type.GetProperty(CreatedAtProperty) != null)
                {
                    var createdAtProperty = entityEntry.Property(CreatedAtProperty);
                    createdAtProperty.CurrentValue = DateTime.Now;
                }
            }
            if (entityEntry.State == EntityState.Modified)
            {
                if (type.GetProperty(CreatedByProperty) != null)
                {
                    var createdByProperty = entityEntry.Property(CreatedByProperty);
                    createdByProperty.IsModified = false;
                }
                if (type.GetProperty(CreatedAtProperty) != null)
                {
                    var createdAtProperty = entityEntry.Property(CreatedAtProperty);
                    createdAtProperty.IsModified = false;
                }
                if (type.GetProperty(LastModifiedByProperty) != null)
                {
                    var modifiedByProperty = entityEntry.Property(LastModifiedByProperty);
                    modifiedByProperty.CurrentValue = userName;
                }
                if (type.GetProperty(LastModifiedAtProperty) != null)
                {
                    var modifiedAtProperty = entityEntry.Property(LastModifiedAtProperty);
                    modifiedAtProperty.CurrentValue = DateTime.Now;
                }
            }
        }
    }
}
