﻿using Customers.Resources.BasicInput;
using Customers.Resources.EmployeeRes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Employees
{
  public  class EmployeesVm
    {

        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "NameAr")]
        public string NameAr { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "NameEn")]
        public string NameEn { get; set; }

        [Display(ResourceType = typeof(EmployeeRes), Name = "WorkSystemId")]
        [UIHint("DropDownList")]
        public int WorkSystemId { get; set; }

       

        [Display(ResourceType = typeof(EmployeeRes), Name = "SpecializationsId")]
        [UIHint("DropDownList")]
        public int SpecializationsId { get; set; }

        [Display(ResourceType = typeof(EmployeeRes), Name = "EmployeeTypeId")]
        [UIHint("DropDownList")]
        public int EmployeeTypeId { get; set; }

        [Display(ResourceType = typeof(EmployeeRes), Name = "EmployeeTypeId")]

        public string EmployeeTypeName { get; set; }

        [Display(ResourceType = typeof(EmployeeRes), Name = "Nationality")]
        public string Nationality { get; set; }

        [Display(ResourceType = typeof(EmployeeRes), Name = "FaceNumber")]
        public string FaceNumber { get; set; }


        [Display(ResourceType = typeof(EmployeeRes), Name = "WorkLocation")]
        public string WorkLocation { get; set; }


        [Display(ResourceType = typeof(EmployeeRes), Name = "WorkHours")]
        public string WorkHours { get; set; }





        [Display(ResourceType = typeof(EmployeeRes), Name = "DateOfAppointment")]

        public DateTime? DateOfAppointment { get; set; }


        [Display(ResourceType = typeof(EmployeeRes), Name = "Salary")]

        public decimal? Salary { get; set; }



        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }


    }
}
