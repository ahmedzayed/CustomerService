﻿using Customers.Resources.BasicInput;
using Customers.Resources.EmployeeRes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Employees
{
 public   class EmployeesConditionsVm
    {
        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(EmployeeRes), Name = "EmployeeId")]

        public int EmployeeId { get; set; }
        [Display(ResourceType = typeof(EmployeeRes), Name = "EmployeeId")]

        public string EmployeeName { get; set; }
        [Display(ResourceType = typeof(EmployeeRes), Name = "CondionId")]
        [UIHint("DropDownList")]

        public int? CondionId { get; set; }
        [Display(ResourceType = typeof(EmployeeRes), Name = "CondionId")]

        public string  ConditionName { get; set; }


        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }

    }
}
