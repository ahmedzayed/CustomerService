﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Resources.BasicInput;
using Customers.Resources.EmployeeRes;
using System;
using System.ComponentModel.DataAnnotations;


namespace Customers.Model.Employees
{
  public  class WorkContractVm
    {
        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "StartDate")]

        public DateTime? StartDate { get; set; }


        public int? EmployeeId { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "EndDate")]

        public DateTime? EndDate { get; set; }


        public string ConditionName { get; set; }


        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
    }
}
