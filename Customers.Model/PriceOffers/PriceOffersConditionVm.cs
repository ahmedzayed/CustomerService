﻿using Customers.Resources.BasicInput;
using Customers.Resources.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.PriceOffers
{
  public  class PriceOffersConditionVm
    {


        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(ContractsRe), Name = "PriceOffersId")]

        public int? PriceOffersId { get; set; }
        [Display(ResourceType = typeof(ContractsRe), Name = "ConditionsId")]
        [UIHint("DropDownList")]

        public int? ConditionsId { get; set; }
        [Display(ResourceType = typeof(ContractsRe), Name = "ConditionsId")]

        public string ConditionsName { get; set; }

        public int CreatedBy { get; set; }


        
    }
}
