﻿using System;

namespace Customers.Model
{
    public class KeyValueLookup
    {
        public int? Value { get; set; }

        public string Text { get; set; }
    }
}
