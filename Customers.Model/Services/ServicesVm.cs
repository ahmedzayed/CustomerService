﻿using Customers.Resources.BasicInput;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.Services
{
  public  class ServicesVm
    {

        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "ServiceNameAr")]
        public string ServiceNameAr { get; set; }




        [Display(ResourceType = typeof(BasicInputRes), Name = "ServiceNameAr")]
        public string ServiceNameEn { get; set; }


        [Display(ResourceType = typeof(BasicInputRes), Name = "Price")]
        public decimal? Price { get; set; }

       

        [Display(ResourceType = typeof(BasicInputRes), Name = "Time")]
        public string Time { get; set; }


        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
    }
}
