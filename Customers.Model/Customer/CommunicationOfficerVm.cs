﻿using Customers.Resources.BasicInput;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.BasicInput
{
   public class CommunicationOfficerVm
    {
        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "NameAr")]
        public string Name { get; set; }

      


        [Display(ResourceType = typeof(BasicInputRes), Name = "JobeName")]
        public string JobeName { get; set; }


        [Display(ResourceType = typeof(BasicInputRes), Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        public int CustomerId { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "Email")]
        public string Email { get; set; }


        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
    }
}
