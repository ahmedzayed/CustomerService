﻿using Customers.Resources.BasicInput;
using Customers.Resources.Customers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.BasicInput
{
   public class CustomerVm
    {

        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "NameAr")]
        public string NameAr { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "NameEn")]
        public string NameEn { get; set; }

        [Display(ResourceType = typeof(CustomerRes), Name = "CustomerActivityId")]
        [UIHint("DropDownList")]
        public int CustomerActivityId { get; set; }

        [Display(ResourceType = typeof(CustomerRes), Name = "CustomerActivityId")]

        public string CustomerActivityNameAr { get; set; }

        [Display(ResourceType = typeof(CustomerRes), Name = "CustomerAddress")]

        public string CustomerAddress { get; set; }

        [Display(ResourceType = typeof(CustomerRes), Name = "CustomerWebSite")]

        public string CustomerWebSite { get; set; }

        [Display(ResourceType = typeof(CustomerRes), Name = "Details")]

        public string Details { get; set; }


        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }


        public List<BasicInputVm> CustomerActivities { get; set; }

    }
}
