﻿using Customers.Resources.BasicInput;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Model.BasicInput
{
    public class BasicInputVm
    {
        [Display(ResourceType = typeof(BasicInputRes), Name = "Id")]
        public int Id { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "NameAr")]
        public string NameAr { get; set; }
        [Display(ResourceType = typeof(BasicInputRes), Name = "NameEn")]
        public string NameEn { get; set; }

        [Display(ResourceType = typeof(BasicInputRes), Name = "Name")]
        public string Name { get; set; }

        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

    }
}
