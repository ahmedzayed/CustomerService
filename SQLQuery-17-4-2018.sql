USE [CustomersServiceDB]
GO
/****** Object:  Table [dbo].[CommunicationOfficer]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunicationOfficer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[JobName] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[CustomerId] [int] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CommunicationOfficer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConditionEmp]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConditionEmp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ConditionEmp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConditionsOffer]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConditionsOffer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Conditions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contracts]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contracts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_Contracts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContractsItems]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractsItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContractsId] [int] NULL,
	[ItemsId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ContractsItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerActivities]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerActivities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](150) NULL,
	[NameEn] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_CustomerActivities_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CustomerActivities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[NameAr] [nvarchar](50) NULL,
	[CustomerActivityId] [int] NULL,
	[CustomerAddress] [nvarchar](50) NULL,
	[CustomerWebSite] [nvarchar](50) NULL,
	[Details] [nvarchar](50) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[NameEn] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerStatus]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](150) NULL,
	[NameEn] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_CustomerStatus_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_CustomerStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[Nationality] [nvarchar](50) NULL,
	[FaceNumber] [nvarchar](50) NULL,
	[WorkSystemId] [int] NULL,
	[WorkLocation] [nvarchar](50) NULL,
	[WorkHours] [nvarchar](50) NULL,
	[DateOfAppointment] [datetime] NULL,
	[Salary] [decimal](18, 0) NULL,
	[SpecializationsId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[EmployeeTypeId] [int] NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeesConditions]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeesConditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NULL,
	[ConditionId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_EmployeesConditions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeSpecializations]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSpecializations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](150) NULL,
	[NameEn] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_EmployeeSpecializations_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_EmployeeSpecializations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeType]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_EmployeeType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Items]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PriceOffersConditions]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceOffersConditions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConditionsId] [int] NULL,
	[PriceOffersId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PriceOffersConditions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PricesOffers]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PricesOffers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](50) NULL,
	[NameEn] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_PricesOffers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Services]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServiceNameAr] [nvarchar](50) NULL,
	[Price] [decimal](18, 0) NULL,
	[Time] [nvarchar](50) NULL,
	[ServiceNameEn] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TasksAnalysis]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TasksAnalysis](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](150) NULL,
	[NameEn] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_TasksAnalysis_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_TasksAnalysis] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TasksEngineers]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TasksEngineers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](150) NULL,
	[NameEn] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_TasksEngineers_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_TasksEngineers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkContract]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkContract](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[EmployeeId] [int] NULL,
 CONSTRAINT [PK_WorkContract] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkSystem]    Script Date: 4/17/2018 7:48:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkSystem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameAr] [nvarchar](150) NULL,
	[NameEn] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_WorkSystem_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_WorkSystem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[CommunicationOfficer] ON 

GO
INSERT [dbo].[CommunicationOfficer] ([Id], [Name], [JobName], [PhoneNumber], [Email], [CustomerId], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (33, N'asasad', N'aada', N'asad', N'asada', 42, NULL, NULL, 0, CAST(N'2018-04-16 10:12:26.690' AS DateTime))
GO
INSERT [dbo].[CommunicationOfficer] ([Id], [Name], [JobName], [PhoneNumber], [Email], [CustomerId], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (35, N'asasad', N'asadad', N'asas', N'asas', 43, NULL, NULL, 0, CAST(N'2018-04-16 10:17:33.123' AS DateTime))
GO
INSERT [dbo].[CommunicationOfficer] ([Id], [Name], [JobName], [PhoneNumber], [Email], [CustomerId], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (37, N'ssdsdccvcv', N'asas', N'asasddddnnn', N'asasasadad', 43, NULL, NULL, 0, CAST(N'2018-04-16 10:18:03.743' AS DateTime))
GO
INSERT [dbo].[CommunicationOfficer] ([Id], [Name], [JobName], [PhoneNumber], [Email], [CustomerId], [UpdatedBy], [UpdatedDate], [CreatedBy], [CreatedDate]) VALUES (39, N'dsdsd', N'sdsd', N'sdsd', N'sdsd', 4, NULL, NULL, 0, CAST(N'2018-04-16 16:14:47.757' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CommunicationOfficer] OFF
GO
SET IDENTITY_INSERT [dbo].[ConditionEmp] ON 

GO
INSERT [dbo].[ConditionEmp] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'asas', N'asaa', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ConditionEmp] OFF
GO
SET IDENTITY_INSERT [dbo].[ConditionsOffer] ON 

GO
INSERT [dbo].[ConditionsOffer] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'a', N'aaaaa', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[ConditionsOffer] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'zzz', N'zzz', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ConditionsOffer] OFF
GO
SET IDENTITY_INSERT [dbo].[Contracts] ON 

GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (2, N'asas', N'asas', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (3, N'asasa', N'asasa', CAST(N'2018-04-17 10:06:04.447' AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (5, N'PPP', N'PPP', CAST(N'2018-04-17 10:06:48.853' AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (6, N'ssssssss', N'ssssssss', CAST(N'2018-04-17 10:13:48.820' AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (7, N'iiuiuiu', N'iiuiuiu', CAST(N'2018-04-17 10:31:18.670' AS DateTime), NULL, NULL, 1)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (8, N'sasas', N'asasas', CAST(N'2018-04-17 12:14:11.147' AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (9, N'ddddd', N'sdsdsd', CAST(N'2018-04-17 12:14:41.597' AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[Contracts] ([Id], [NameAr], [NameEn], [CreatedDate], [UpdatedDate], [UpdatedBy], [CreatedBy]) VALUES (10, N'سسس', N'سسسس', CAST(N'2018-04-17 18:54:54.877' AS DateTime), NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Contracts] OFF
GO
SET IDENTITY_INSERT [dbo].[ContractsItems] ON 

GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, 6, 13, 0, CAST(N'2018-04-17 10:13:49.597' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, 6, 11, 0, CAST(N'2018-04-17 10:20:48.747' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, 7, 11, 0, CAST(N'2018-04-17 10:31:24.740' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, 7, 11, 0, CAST(N'2018-04-17 10:37:26.773' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, 3, 11, 0, CAST(N'2018-04-17 16:49:12.303' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, 2, 11, 0, CAST(N'2018-04-17 16:49:41.947' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[ContractsItems] ([Id], [ContractsId], [ItemsId], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, 10, 11, 0, CAST(N'2018-04-17 18:54:55.057' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ContractsItems] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerActivities] ON 

GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'نشاط 1', N'نشاط 1', CAST(N'2018-04-14 15:03:15.463' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1002, N'ggg', N'gggg', CAST(N'2018-04-16 10:39:11.300' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1003, N'ggg', N'ggg', CAST(N'2018-04-16 10:39:25.327' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1004, N'dd', N'dddd', CAST(N'2018-04-16 10:48:50.083' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1005, N'vv', N'vvv', CAST(N'2018-04-16 10:49:02.573' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1006, N'bb', N'bbb', CAST(N'2018-04-16 10:49:57.087' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1007, N'ؤؤ', N'ؤؤؤ', CAST(N'2018-04-16 11:38:25.753' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1008, N'رر', N'ررر', CAST(N'2018-04-16 11:42:31.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[CustomerActivities] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1009, N'gg', N'ggg', CAST(N'2018-04-16 11:56:07.677' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[CustomerActivities] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasfffff', 4, N'sasas', N'asas', NULL, 4, CAST(N'2018-04-15 15:02:39.703' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasas', 4, N'asasasas', N'asas', N'asasas', 6, CAST(N'2018-04-15 15:12:12.527' AS DateTime), 0, NULL, NULL, N'aasas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'هاجر', 4, N'شسشس', N'شسشس', N'شسشس', 7, CAST(N'2018-04-15 16:56:50.970' AS DateTime), 0, NULL, NULL, N'شسشس')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'zzzzz', 4, N'zz', N'zzz', N'zzz', 8, CAST(N'2018-04-15 16:59:11.850' AS DateTime), 0, NULL, NULL, N'zzz')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasddd', 4, N'asas', N'asas', N'                                            ', 9, CAST(N'2018-04-15 17:05:33.073' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'nnnn', 4, N'asas', N'asas', N'asass                                            ', 10, CAST(N'2018-04-15 17:06:58.423' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 4, N'sasas', N'asas', N'asasas', 11, CAST(N'2018-04-15 15:02:39.703' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasas', 4, N'asasasas', N'asas', N'asasas', 12, CAST(N'2018-04-15 15:12:12.527' AS DateTime), 0, NULL, NULL, N'aasas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'هاجر', 4, N'شسشس', N'شسشس', N'شسشس', 13, CAST(N'2018-04-15 16:56:50.970' AS DateTime), 0, NULL, NULL, N'شسشس')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'zzzzz', 4, N'zz', N'zzz', N'zzz', 14, CAST(N'2018-04-15 16:59:11.850' AS DateTime), 0, NULL, NULL, N'zzz')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasddd', 4, N'asas', N'asas', N'                                            ', 15, CAST(N'2018-04-15 17:05:33.073' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'nnnn', 4, N'asas', N'asas', N'asass                                            ', 16, CAST(N'2018-04-15 17:06:58.423' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 17, CAST(N'2018-04-16 09:32:57.377' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 18, CAST(N'2018-04-16 09:33:10.067' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 19, CAST(N'2018-04-16 09:33:10.227' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 20, CAST(N'2018-04-16 09:33:10.427' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 21, CAST(N'2018-04-16 09:33:17.787' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 22, CAST(N'2018-04-16 09:33:28.517' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 23, CAST(N'2018-04-16 09:33:28.687' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 24, CAST(N'2018-04-16 09:33:28.837' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 25, CAST(N'2018-04-16 09:33:29.000' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 26, CAST(N'2018-04-16 09:33:29.170' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 27, CAST(N'2018-04-16 09:33:29.453' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 28, CAST(N'2018-04-16 09:33:29.717' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 29, CAST(N'2018-04-16 09:33:29.950' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'aaaaaa', 3, N'asas', N'asas', N'asas', 30, CAST(N'2018-04-16 09:34:32.483' AS DateTime), 0, NULL, NULL, N'aaaaaa')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'aaaaaazzz', 3, N'asas', N'asas', N'asas', 31, CAST(N'2018-04-16 09:34:39.277' AS DateTime), 0, NULL, NULL, N'aaaaaa')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'aaaaa', 3, N'asas', N'asas', N'asas', 32, CAST(N'2018-04-16 09:36:06.140' AS DateTime), 0, NULL, NULL, N'aaaaaa')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'aa', 3, N'asas', N'asas', N'asasas', 33, CAST(N'2018-04-16 09:36:49.783' AS DateTime), 0, NULL, NULL, N'aa')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'aa', 3, N'asas', N'asas', N'asasas', 34, CAST(N'2018-04-16 09:36:51.483' AS DateTime), 0, NULL, NULL, N'aa')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asas', 35, CAST(N'2018-04-16 09:40:08.210' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasasad', 3, N'asas', N'asas', N'asas', 36, CAST(N'2018-04-16 09:55:17.693' AS DateTime), 0, NULL, NULL, N'adas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasasad', 3, N'asas', N'asas', N'asas', 37, CAST(N'2018-04-16 09:55:24.250' AS DateTime), 0, NULL, NULL, N'adas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasasad', 3, N'asas', N'asas', N'asas', 38, CAST(N'2018-04-16 10:01:01.470' AS DateTime), 0, NULL, NULL, N'adas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'ass', N'asas', N'asasas', 39, CAST(N'2018-04-16 10:01:41.497' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'ass', N'asas', N'asasas', 40, CAST(N'2018-04-16 10:03:09.657' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asas', 3, N'asas', N'asas', N'asasas', 41, CAST(N'2018-04-16 10:03:33.087' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasad', 3, N'asas', N'asas', N'asasas', 42, CAST(N'2018-04-16 10:12:16.623' AS DateTime), 0, NULL, NULL, N'adad')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'wwwwww', 3, N'asas', N'asasas', N'asasas', 43, CAST(N'2018-04-16 10:17:32.727' AS DateTime), 0, NULL, NULL, N'www')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasad', 4, N'asas', N'asas', N'asasad', 44, CAST(N'2018-04-16 10:22:22.000' AS DateTime), 0, NULL, NULL, N'asas')
GO
INSERT [dbo].[Customers] ([NameAr], [CustomerActivityId], [CustomerAddress], [CustomerWebSite], [Details], [Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [NameEn]) VALUES (N'asasadnnnn', 4, N'asas', N'asas', NULL, 45, CAST(N'2018-04-16 10:26:03.397' AS DateTime), 0, NULL, NULL, N'asas')
GO
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerStatus] ON 

GO
INSERT [dbo].[CustomerStatus] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N' gg5566', N'ggg5566', CAST(N'2018-04-16 10:52:19.063' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[CustomerStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 

GO
INSERT [dbo].[Employees] ([Id], [NameAr], [NameEn], [Nationality], [FaceNumber], [WorkSystemId], [WorkLocation], [WorkHours], [DateOfAppointment], [Salary], [SpecializationsId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [EmployeeTypeId]) VALUES (2, N'asasas', N'asas', N'asas', N'asas', 1, N'asasas', N'asasas', CAST(N'2018-04-01 00:00:00.000' AS DateTime), CAST(12 AS Decimal(18, 0)), 1, CAST(N'2018-04-17 14:53:47.383' AS DateTime), 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Employees] ([Id], [NameAr], [NameEn], [Nationality], [FaceNumber], [WorkSystemId], [WorkLocation], [WorkHours], [DateOfAppointment], [Salary], [SpecializationsId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [EmployeeTypeId]) VALUES (3, N'aaaaa', N'asas', N'asas', N'asas', 2, N'asas', N'aasas', CAST(N'2018-01-01 00:00:00.000' AS DateTime), CAST(4000 AS Decimal(18, 0)), 1, CAST(N'2018-04-17 14:56:13.410' AS DateTime), 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Employees] ([Id], [NameAr], [NameEn], [Nationality], [FaceNumber], [WorkSystemId], [WorkLocation], [WorkHours], [DateOfAppointment], [Salary], [SpecializationsId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [EmployeeTypeId]) VALUES (4, N'wwwwwww', N'wewe', N'asas', N'asas', 1, N'asas', N'asas', CAST(N'2018-04-02 00:00:00.000' AS DateTime), CAST(500 AS Decimal(18, 0)), 1, CAST(N'2018-04-17 15:29:29.627' AS DateTime), 0, NULL, NULL, NULL)
GO
INSERT [dbo].[Employees] ([Id], [NameAr], [NameEn], [Nationality], [FaceNumber], [WorkSystemId], [WorkLocation], [WorkHours], [DateOfAppointment], [Salary], [SpecializationsId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [EmployeeTypeId]) VALUES (7, N'asas', N'asas', N'asas', N'asas', 1, N'asas', N'44', CAST(N'2018-04-02 00:00:00.000' AS DateTime), CAST(55 AS Decimal(18, 0)), 1, CAST(N'2018-04-17 15:33:08.477' AS DateTime), 0, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Employees] OFF
GO
SET IDENTITY_INSERT [dbo].[EmployeesConditions] ON 

GO
INSERT [dbo].[EmployeesConditions] ([Id], [EmployeeId], [ConditionId], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy]) VALUES (2, 4, 1, 0, CAST(N'2018-04-17 15:29:47.717' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[EmployeesConditions] ([Id], [EmployeeId], [ConditionId], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy]) VALUES (3, 3, 1, 0, CAST(N'2018-04-17 15:30:47.597' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[EmployeesConditions] ([Id], [EmployeeId], [ConditionId], [CreatedBy], [CreatedDate], [UpdatedDate], [UpdatedBy]) VALUES (4, 7, 1, 0, CAST(N'2018-04-17 15:33:48.157' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[EmployeesConditions] OFF
GO
SET IDENTITY_INSERT [dbo].[EmployeeSpecializations] ON 

GO
INSERT [dbo].[EmployeeSpecializations] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'تخصص1', N'تخصص1', CAST(N'2018-04-16 11:23:31.987' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[EmployeeSpecializations] OFF
GO
SET IDENTITY_INSERT [dbo].[EmployeeType] ON 

GO
INSERT [dbo].[EmployeeType] ([Id], [Type]) VALUES (1, N'مندوب')
GO
INSERT [dbo].[EmployeeType] ([Id], [Type]) VALUES (2, N'مهندس')
GO
INSERT [dbo].[EmployeeType] ([Id], [Type]) VALUES (3, N'اداري')
GO
SET IDENTITY_INSERT [dbo].[EmployeeType] OFF
GO
SET IDENTITY_INSERT [dbo].[Items] ON 

GO
INSERT [dbo].[Items] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, N'sdsd', N'asasa', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Items] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (13, N'wewe', N'wewe', CAST(N'2018-04-16 17:29:58.430' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Items] OFF
GO
SET IDENTITY_INSERT [dbo].[PriceOffersConditions] ON 

GO
INSERT [dbo].[PriceOffersConditions] ([Id], [ConditionsId], [PriceOffersId], [CreatedDate], [CreatedBy], [UpdateDate], [UpdatedBy]) VALUES (1, 1, 2, CAST(N'2018-04-17 12:26:54.830' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[PriceOffersConditions] ([Id], [ConditionsId], [PriceOffersId], [CreatedDate], [CreatedBy], [UpdateDate], [UpdatedBy]) VALUES (2, 2, 4, CAST(N'2018-04-17 12:28:22.943' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[PriceOffersConditions] ([Id], [ConditionsId], [PriceOffersId], [CreatedDate], [CreatedBy], [UpdateDate], [UpdatedBy]) VALUES (6, 1, 3, CAST(N'2018-04-17 12:38:59.127' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[PriceOffersConditions] OFF
GO
SET IDENTITY_INSERT [dbo].[PricesOffers] ON 

GO
INSERT [dbo].[PricesOffers] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'sssssssss', N'asasas', 0, CAST(N'2018-04-17 12:15:53.587' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[PricesOffers] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'cccccccc', N'asas', 0, CAST(N'2018-04-17 12:26:54.420' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[PricesOffers] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'eeeee', N'aqsasas', 0, CAST(N'2018-04-17 12:27:47.800' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[PricesOffers] ([Id], [NameAr], [NameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'eeeee', N'aqsasas', 0, CAST(N'2018-04-17 12:27:58.840' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[PricesOffers] OFF
GO
SET IDENTITY_INSERT [dbo].[Services] ON 

GO
INSERT [dbo].[Services] ([Id], [ServiceNameAr], [Price], [Time], [ServiceNameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'sas', CAST(40 AS Decimal(18, 0)), N'asas', N'asas', 0, CAST(N'2018-04-16 16:29:34.450' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Services] ([Id], [ServiceNameAr], [Price], [Time], [ServiceNameEn], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'asas', CAST(500 AS Decimal(18, 0)), N'aaa', N'asas', 0, CAST(N'2018-04-16 16:33:03.903' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Services] OFF
GO
SET IDENTITY_INSERT [dbo].[TasksAnalysis] ON 

GO
INSERT [dbo].[TasksAnalysis] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'ببب2', N'ببب2', CAST(N'2018-04-16 11:10:39.057' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TasksAnalysis] OFF
GO
SET IDENTITY_INSERT [dbo].[TasksEngineers] ON 

GO
INSERT [dbo].[TasksEngineers] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'مهمة12', N'مهمة12', CAST(N'2018-04-16 11:04:52.557' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TasksEngineers] OFF
GO
SET IDENTITY_INSERT [dbo].[WorkSystem] ON 

GO
INSERT [dbo].[WorkSystem] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'نظام 24', N'نظام 24', CAST(N'2018-04-16 11:29:44.707' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[WorkSystem] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'asasas', N'asasa', CAST(N'2018-04-16 17:10:24.487' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[WorkSystem] ([Id], [NameAr], [NameEn], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'sdsd', N'sssd', CAST(N'2018-04-16 17:12:39.193' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[WorkSystem] OFF
GO
ALTER TABLE [dbo].[CommunicationOfficer]  WITH CHECK ADD  CONSTRAINT [FK_CommunicationOfficer_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[CommunicationOfficer] CHECK CONSTRAINT [FK_CommunicationOfficer_Customers]
GO
ALTER TABLE [dbo].[ContractsItems]  WITH CHECK ADD  CONSTRAINT [FK_ContractsItems_Contracts] FOREIGN KEY([ContractsId])
REFERENCES [dbo].[Contracts] ([Id])
GO
ALTER TABLE [dbo].[ContractsItems] CHECK CONSTRAINT [FK_ContractsItems_Contracts]
GO
ALTER TABLE [dbo].[ContractsItems]  WITH CHECK ADD  CONSTRAINT [FK_ContractsItems_Items] FOREIGN KEY([ItemsId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[ContractsItems] CHECK CONSTRAINT [FK_ContractsItems_Items]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_EmployeeSpecializations] FOREIGN KEY([SpecializationsId])
REFERENCES [dbo].[EmployeeSpecializations] ([Id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_EmployeeSpecializations]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_EmployeeType] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([Id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_EmployeeType]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_WorkSystem] FOREIGN KEY([WorkSystemId])
REFERENCES [dbo].[WorkSystem] ([Id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_WorkSystem]
GO
ALTER TABLE [dbo].[EmployeesConditions]  WITH CHECK ADD  CONSTRAINT [FK_EmployeesConditions_Conditions] FOREIGN KEY([ConditionId])
REFERENCES [dbo].[ConditionEmp] ([Id])
GO
ALTER TABLE [dbo].[EmployeesConditions] CHECK CONSTRAINT [FK_EmployeesConditions_Conditions]
GO
ALTER TABLE [dbo].[EmployeesConditions]  WITH CHECK ADD  CONSTRAINT [FK_EmployeesConditions_Employees] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employees] ([Id])
GO
ALTER TABLE [dbo].[EmployeesConditions] CHECK CONSTRAINT [FK_EmployeesConditions_Employees]
GO
ALTER TABLE [dbo].[PriceOffersConditions]  WITH CHECK ADD  CONSTRAINT [FK_PriceOffersConditions_Conditions] FOREIGN KEY([ConditionsId])
REFERENCES [dbo].[ConditionsOffer] ([Id])
GO
ALTER TABLE [dbo].[PriceOffersConditions] CHECK CONSTRAINT [FK_PriceOffersConditions_Conditions]
GO
ALTER TABLE [dbo].[PriceOffersConditions]  WITH CHECK ADD  CONSTRAINT [FK_PriceOffersConditions_PricesOffers] FOREIGN KEY([PriceOffersId])
REFERENCES [dbo].[PricesOffers] ([Id])
GO
ALTER TABLE [dbo].[PriceOffersConditions] CHECK CONSTRAINT [FK_PriceOffersConditions_PricesOffers]
GO
ALTER TABLE [dbo].[WorkContract]  WITH CHECK ADD  CONSTRAINT [FK_WorkContract_Employees] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employees] ([Id])
GO
ALTER TABLE [dbo].[WorkContract] CHECK CONSTRAINT [FK_WorkContract_Employees]
GO
