﻿var ManageEmployees = {
    init: function () {
        Search();
    },
}
function Save() {

    var $form = $("#ModelForm");
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                //window.location = "../ManageEmployees/Index";
                toastr.success(res[1]);
                Search();
            }
            else
                toastr.error(res[1]);
        });
        //  }
    }
}
function Create() {
    var Url = "../ManageEmployees/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

function WorkContract(id)
{
    var Url = "../ManageEmployees/AddWorkContract?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });

}

function Edit(id) {
    var Url = "../ManageEmployees/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../ManageEmployees/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../ManageEmployees/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageEmployees/Index";
        }
        else
            toastr.error(res[1]);
    });
}




function DeleteItems(id) {
    var Url = "../ManageEmployees/DeleteItems?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

function DeleteItemsRow(id) {

    var Url = "../ManageEmployees/DeleteItemsRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            //Search();
            window.location = "../ManageEmployees/Index1";
        }
        else
            toastr.error(res[1]);
    });
}



function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../ManageEmployees/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


