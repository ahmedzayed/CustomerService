﻿using Customers.Portal.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    public class SharedController : BaseController
    {
        // GET: BasicInput/Shared
        public ActionResult _SideMenu()
        {
            return PartialView();
        }
    }
}