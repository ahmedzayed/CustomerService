﻿using Customers.Abstracts;
using Customers.Abstracts.BasicInput;
using Customers.Model.BasicInput;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    public class ManageWorkSystemController : BaseController
    {
        #region Services

        private readonly IWorkSystemService _thisService;
        private readonly CommonService _commonService;

		#endregion

		#region Constructor
        public ManageWorkSystemController(IWorkSystemService thisService, ILookupService lookupService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);
		}
		#endregion


        #region Actions

        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult Search(BasicInputSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            BasicInputVm Obj = new BasicInputVm();
            return PartialView("~/Areas/BasicInput/Views/ManageWorkSystem/Create.cshtml",Obj);
        }
        [HttpPost]
        public JsonResult Create(BasicInputVm viewModel)
        {
                if (viewModel.NameEn == null || viewModel.NameEn == "")
                    viewModel.NameEn = viewModel.NameAr;

                if (_thisService.Save(viewModel))
                {
                    return Json("success," +GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int id = 0)
        {
            BasicInputVm obj = _thisService.GetById(id);
            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManageWorkSystem/Create.cshtml", obj);
        }

        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BasicInputVm obj = new BasicInputVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageWorkSystem/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
	}
}