﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Customers.Portal.Controllers;
using Customers.Abstracts.Employees;
using Customers.Model.Employees;
using Customers.Services.Employees;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    public class ManageEmployeesController : BaseController
    {
        #region Service

        private readonly IEmployeesService _thisService;
        private readonly IEmployeesConditionService _EmployeesConditionService;
        private readonly IWorkContractService _WorkContractService;
        private readonly Utilities.CommonService _commonService;
        #endregion


        #region Constructor
        public ManageEmployeesController(IEmployeesService thisService, IWorkContractService WorkContractService, Abstracts.ILookupService lookupService, IEmployeesConditionService EmployeesConditionService)
        {
            _thisService = thisService;
            _commonService = new Utilities.CommonService(lookupService);
            _EmployeesConditionService = EmployeesConditionService;
            _WorkContractService = WorkContractService;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(EmployeesSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }


        public ActionResult AddWorkContract(int id)
        {
            ViewBag.EmployeeId = id;
            WorkContractVm obj = new WorkContractVm();
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/CreateWorkContract.cshtml",obj);


        }
        public ActionResult Create()
        {
            EmployeesVm Obj = new EmployeesVm();
            ViewBag.SpecializationsId = _commonService.FindSpecialization();
            ViewBag.WorkSystemId = _commonService.FindWorkSystem();
            ViewBag.EmployeeTypeId = _commonService.FindEmployeeType();
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/Create.cshtml", Obj);
        }




        [HttpPost]
        public JsonResult Create(EmployeesVm viewModel, int? CondionId)
        {
            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;

            if(viewModel.Id != 0)
            {
                if(CondionId != 0)
                {
                    EmployeesConditionsVm C = new EmployeesConditionsVm();
                    C.EmployeeId = viewModel.Id; /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
                    C.CondionId = CondionId;

                    _EmployeesConditionService.Save(C);
                    return Json("success," + Resources.GlobalRes.GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + Resources.GlobalRes.GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);

            }
            else
            {

                if (_thisService.Save(viewModel))
                {
                    if (CondionId != 0)
                    {
                        EmployeesConditionsVm C = new EmployeesConditionsVm();
                        C.EmployeeId = _thisService.FindLast(viewModel).LastOrDefault().Id;
                        C.CondionId = CondionId;

                        _EmployeesConditionService.Save(C);
                        return Json("success," + Resources.GlobalRes.GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                    }
                    return Json("success," + Resources.GlobalRes.GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + Resources.GlobalRes.GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }



            //if (_thisService.Save(viewModel))
            //{
            //    return Json("success," + Resources.GlobalRes.GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);
            //}
            //else
            //    return Json("error," + Resources.GlobalRes.GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            EmployeesVm obj = _thisService.GetById(id);
            ViewBag.SpecializationsId = _commonService.FindSpecialization(obj.SpecializationsId);
            ViewBag.WorkSystemId = _commonService.FindWorkSystem(obj.WorkSystemId);
            ViewBag.EmployeeTypeId = _commonService.FindEmployeeType(obj.EmployeeTypeId);

            //ViewBag.CustomerActivityId = _commonService.FindCustomerActivities(obj.CustomerActivityId);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            EmployeesVm obj = new EmployeesVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + Resources.GlobalRes.GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + Resources.GlobalRes.GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Condions
        public ActionResult GetConditionsEmp()
        {
            ViewBag.CondionId = _commonService.FindConditionsEmp();

            return PartialView("_Conditions");
        }


        public ActionResult GetCondionsEmpList(int? Id)
        {
            EmployeesConditionsSm searchModel = new EmployeesConditionsSm();


            var Data = _EmployeesConditionService.GetByEmployeeId(Id);

            return PartialView("_GetConditionsEmpList", Data);
        }


        public ActionResult GetWorkContract(int? EmpId)
        {

            WorkContractSm searchModel = new WorkContractSm();


            var Data = _WorkContractService.GetByEmployeeId(EmpId);

            return PartialView("_GetWorkContractList", Data);
        }

        public ActionResult DeleteItems(int id)
        {
            EmployeesConditionsVm obj = new EmployeesConditionsVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageEmployees/DeleteItems.cshtml", obj);
        }


        public JsonResult DeleteItemsRow(int Id)
        {

            if (_EmployeesConditionService.Delete(Id))
            {



                return Json("success," + Resources.GlobalRes.GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + Resources.GlobalRes.GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}