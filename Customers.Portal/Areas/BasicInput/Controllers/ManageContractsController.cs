﻿using Customers.Abstracts;
using Customers.Abstracts.Contracts;
using Customers.Model.BasicInput;
using Customers.Model.Contracts;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    public class ManageContractsController : BaseController
    {
        #region Service

        private readonly IContractsService _thisService;
        private readonly IContractsItemsService _ContractsItemsService;
        private readonly CommonService _commonService;
        #endregion


        #region Constructor
        public ManageContractsController(IContractsService thisService, ILookupService lookupService, IContractsItemsService ContractsItemsService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);
            _ContractsItemsService = ContractsItemsService;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(BasicInputSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            BasicInputVm Obj = new BasicInputVm();

            return PartialView("~/Areas/BasicInput/Views/ManageContracts/Create.cshtml", Obj);
        }


        public ActionResult GetItems()
        {
            ViewBag.ItemsId = _commonService.FindItems();

            return PartialView("_Items");
        }


        [HttpPost]
        public JsonResult Create(BasicInputVm viewModel ,int? ItemsId)
        {

            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;


            if (viewModel.Id != 0)
            {
                viewModel.Name = viewModel.NameAr;


                
                    if (ItemsId != 0)
                    {
                        ContractsItemsVm C = new ContractsItemsVm();
                        C.ContractsId = viewModel.Id; /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
                        C.ItemsId = ItemsId;

                        _ContractsItemsService.Save(C);
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }


                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }

            else
            {
                if (_thisService.Save(viewModel))
                {
                    if (ItemsId != 0)
                    {
                        ContractsItemsVm C = new ContractsItemsVm();
                        C.ContractsId = _thisService.FindLast(viewModel).LastOrDefault().Id;
                        C.ItemsId = ItemsId;

                        _ContractsItemsService.Save(C);

                    }
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Edit(int id = 0)
        {
            BasicInputVm obj = _thisService.GetById(id);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManageContracts/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BasicInputVm obj = new BasicInputVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageContracts/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Items
        public ActionResult GetItemsList(int? Id)
        {
            ContractsItemsSm searchModel = new ContractsItemsSm();


            var Data = _ContractsItemsService.GetByContractsId(Id);

            return PartialView("_GetItemsList", Data);
        }


        public ActionResult DeleteItems(int id)
        {
            ContractsItemsVm obj = new ContractsItemsVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManageContracts/DeleteItems.cshtml", obj);
        }


        public JsonResult DeleteItemsRow(int Id)
        {

            if (_ContractsItemsService.Delete(Id))
            {



                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}