﻿using Customers.Abstracts;
using Customers.Abstracts.PriceOffers;
using Customers.Model.BasicInput;
using Customers.Model.Contracts;
using Customers.Model.PriceOffers;
using Customers.Portal.Controllers;
using Customers.Portal.Utilities;
using Customers.Resources.GlobalRes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Customers.Portal.Areas.BasicInput.Controllers
{
    public class ManagePricesOffersController : BaseController
    {
        #region Service

        private readonly IPriceOffersService _thisService;
        private readonly IPriceOffersConditionsService _PriceOffersConditionsService;
        private readonly CommonService _commonService;
        #endregion


        #region Constructor
        public ManagePricesOffersController(IPriceOffersService thisService, ILookupService lookupService, IPriceOffersConditionsService PriceOffersConditionsService)
        {
            _thisService = thisService;
            _commonService = new CommonService(lookupService);
            _PriceOffersConditionsService = PriceOffersConditionsService;
        }
        #endregion

        #region Actions

        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search(BasicInputSm searchModel)
        {
            var result = _thisService.FindAll(searchModel);
            return PartialView(result);
        }

        public ActionResult Create()
        {
            BasicInputVm Obj = new BasicInputVm();

            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/Create.cshtml", Obj);
        }


        public ActionResult GetConditions()
        {
            ViewBag.ConditionsId = _commonService.FindConditions();

            return PartialView("_Conditions");
        }


        [HttpPost]
        public JsonResult Create(BasicInputVm viewModel, int? ConditionsId)
        {

            if (viewModel.NameEn == null || viewModel.NameEn == "")
                viewModel.NameEn = viewModel.NameAr;


            if (viewModel.Id != 0)
            {
                viewModel.Name = viewModel.NameAr;



                if (ConditionsId != 0)
                {
                    PriceOffersConditionVm C = new PriceOffersConditionVm();
                    C.PriceOffersId = viewModel.Id; /*_thisService.FindLast(viewModel).LastOrDefault().Id*/;
                    C.ConditionsId = ConditionsId;

                    _PriceOffersConditionsService.Save(C);
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }


                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }

            else
            {
                if (_thisService.Save(viewModel))
                {
                    if (ConditionsId != 0)
                    {
                        PriceOffersConditionVm C = new PriceOffersConditionVm();
                        C.PriceOffersId = _thisService.FindLast(viewModel).LastOrDefault().Id;
                        C.ConditionsId = ConditionsId;

                        _PriceOffersConditionsService.Save(C);

                    }
                    return Json("success," + GlobalRes.MessageSuccuss.ToString(), JsonRequestBehavior.AllowGet);

                }
                else
                    return Json("error," + GlobalRes.MessageError.ToString(), JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult Edit(int id = 0)
        {
            BasicInputVm obj = _thisService.GetById(id);

            if (obj == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/Create.cshtml", obj);
        }


        public ActionResult Details(int id)
        {
            return PartialView(_thisService.GetById(id));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BasicInputVm obj = new BasicInputVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/Delete.cshtml", obj);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            if (_thisService.Delete(Id))
            {
                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Items
        public ActionResult GetCondionsList(int? Id)
        {
            //ContractsItemsSm searchModel = new ContractsItemsSm();


            var Data = _PriceOffersConditionsService.GetByPriceOffersId(Id);

            return PartialView("_GetConditionsList", Data);
        }


        public ActionResult DeleteItems(int id)
        {
            ContractsItemsVm obj = new ContractsItemsVm();
            obj.Id = id;
            return PartialView("~/Areas/BasicInput/Views/ManagePricesOffers/DeleteItems.cshtml", obj);
        }


        public JsonResult DeleteItemsRow(int Id)
        {

            if (_PriceOffersConditionsService.Delete(Id))
            {



                return Json("success," + GlobalRes.Messagedelete, JsonRequestBehavior.AllowGet);

            }
            else
                return Json("error," + GlobalRes.MessageError, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}