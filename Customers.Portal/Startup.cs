﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Customers.Portal.Startup))]
namespace Customers.Portal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
