[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Customers.Portal.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Customers.Portal.App_Start.NinjectWebCommon), "Stop")]

namespace Customers.Portal.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Customers.Abstracts;
    using Customers.Abstracts.BasicInput;
    using Customers.Services.BasicInput;
    using Bank.Services;
    using Abstracts.Customer;
    using Abstracts.Employees;
    using Abstracts.Contracts;
    using Services.Contracts;
    using Abstracts.PriceOffers;
    using Services.PriceOffers;
    using Services.Employees;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                kernel.Bind<ILookupService>().To<LookupService>();
                kernel.Bind<ICustomerStatusService>().To<CustomerStatusService>();
                kernel.Bind<ITasksEngineersService>().To<TasksEngineersService>();
                kernel.Bind<ITasksAnalysisService>().To<TasksAnalysisService>();
                kernel.Bind<IEmployeeSpecializationsService>().To<EmployeeSpecializationsService>();
                kernel.Bind<IWorkSystemService>().To<WorkSystemService>();

                kernel.Bind<ICustomerActivitiesService>().To<CustomerActivitiesService>();
                kernel.Bind<ICustomerService>().To<CustomerService>();
                kernel.Bind<ICommunicationOfficerService>().To<CommunicationOfficerService>();
                kernel.Bind<IServicesService>().To<ServicesService>();
                kernel.Bind<IItemsService>().To<ItemsService>();
                kernel.Bind<IContractsService>().To<ContractsService>();
                kernel.Bind<IContractsItemsService>().To<ContractsItemsService>();

                kernel.Bind<IPriceOffersService>().To<PriceOffersService>();
                kernel.Bind<IPriceOffersConditionsService>().To<PriceOffersCondionsService>();

                kernel.Bind<IEmployeesService>().To<EmployeesService>();
                kernel.Bind<IEmployeesConditionService>().To<EmployeesConditionService>();
                kernel.Bind<IWorkContractService>().To<WorkContractService>();



                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
        }        
    }
}
