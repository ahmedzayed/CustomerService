﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Customers.Abstracts;
using Customers.Services;
using Customers.Model;
using Customers.Helpers.Utilities;

namespace Bank.Services
{
    public class LookupService : BaseService, ILookupService
    {

        #region BasicInput
        public List<KeyValueLookup> FindCustomerActivities()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  النشاط --" };
                Items = Context.CustomerActivities.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Activity  --" };
                Items = Context.CustomerActivities.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindWorkSystem()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  نظام العمل --" };
                Items = Context.WorkSystem.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select WorkSystem  --" };
                Items = Context.WorkSystem.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindEmployeeType()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر   نوع الموظف --" };
                Items = Context.EmployeeType.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.Type
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select EmployeeType  --" };
                Items = Context.EmployeeType.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.Type
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        public List<KeyValueLookup> FindSpecialization()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر   التخصص --" };
                Items = Context.EmployeeSpecializations.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Specialization  --" };
                Items = Context.EmployeeSpecializations.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindItems()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  البند --" };
                Items = Context.Items.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Items  --" };
                Items = Context.Items.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }
        public List<KeyValueLookup> FindConditions()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  الشرط --" };
                Items = Context.ConditionsOffer.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Items  --" };
                Items = Context.ConditionsOffer.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        public List<KeyValueLookup> FindConditionsEmp()
        {
            KeyValueLookup FirstItem;
            List<KeyValueLookup> Items;

            if (ResourcesReader.IsArabic)
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- أختر  الشرط --" };
                Items = Context.ConditionEmp.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameAr
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            else
            {
                FirstItem = new KeyValueLookup { Value = 0, Text = "-- Select Conditions  --" };
                Items = Context.ConditionEmp.Select(m => new KeyValueLookup
                {
                    Value = m.Id,
                    Text = m.NameEn
                }).ToList();
                Items.Insert(0, FirstItem);
            }
            return Items;
        }

        #endregion

    }
}
