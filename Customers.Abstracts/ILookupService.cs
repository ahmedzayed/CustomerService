﻿using Customers.Model;
using System.Collections.Generic;


namespace Customers.Abstracts
{
    public interface ILookupService
    {

        #region BasicInput
        List<KeyValueLookup> FindCustomerActivities();
        List<KeyValueLookup> FindItems();
        List<KeyValueLookup> FindConditions();
        List<KeyValueLookup> FindConditionsEmp();


        List<KeyValueLookup> FindWorkSystem();
        List<KeyValueLookup> FindEmployeeType();

        List<KeyValueLookup> FindSpecialization();

        #endregion
    }
}
