﻿using Customers.Model.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.BasicInput
{
   public interface IServicesService
    {
        ServicesVm GetById(int id);
        bool Save(ServicesVm viewModel);
        List<ServicesVm> FindAll(ServicesSm searchModel);
        bool Delete(int id);
    }
}
