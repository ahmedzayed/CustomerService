﻿using Customers.Model.BasicInput;
using Customers.Model.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.Customer
{
   public interface ICustomerService
    {
        CustomerVm GetById(int id);

        bool Save(CustomerVm viewModel);
        List<CustomerVm> FindAll(CustomerSm searchModel);
        bool Delete(int id);
        List<CustomerVm> FindLast(CustomerVm searchModel);

    }
}
