﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Model.Employees;
namespace Customers.Abstracts.Employees
{
    public interface IEmployeesConditionService
    {
        EmployeesConditionsVm GetById(int id);

        bool Save(EmployeesConditionsVm viewModel);
        List<EmployeesConditionsVm> FindAll(EmployeesConditionsSm searchModel);
        bool Delete(int id);
        List<EmployeesConditionsVm> GetByEmployeeId(int? id);
    }
}
