﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customers.Model.Employees;

namespace Customers.Abstracts.Employees
{
    public interface IEmployeesService
    {
        EmployeesVm GetById(int id);

        bool Save(EmployeesVm viewModel);
        List<EmployeesVm> FindAll(EmployeesSm searchModel);
        bool Delete(int id);
        List<EmployeesVm> FindLast(EmployeesVm searchModel);
    }
}
