﻿using Customers.Model.PriceOffers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customers.Abstracts.PriceOffers
{
  public  interface IPriceOffersConditionsService
    {
        PriceOffersConditionVm GetById(int id);
        bool Save(PriceOffersConditionVm viewModel);
        List<PriceOffersConditionVm> FindAll(PriceOffersConditionSm searchModel);
        bool Delete(int id);
        List<PriceOffersConditionVm> GetByPriceOffersId(int? id);
    }
}
